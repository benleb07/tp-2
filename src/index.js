import './index.html';
import './style.css';
import mainImage from '../img/loading.png'

import appConfig from '../app.config';
import app from './App/App';

app.start();
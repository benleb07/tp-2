import app from './App';

const mapbox = require('mapbox-gl/dist/mapbox-gl.js');

export class LocalEvt {
    title;
    description;
    dateFrom;
    dateTo;
    lng;
    lat;
    // timeLeft;

    domContainer;
    domTitle;
    domContent;
    domTime;
    domTime2;

    constructor( newEvt ){
        this.title = newEvt.title;
        this.description = newEvt.description;
        this.dateFrom = newEvt.dateFrom;
        this.dateTo = newEvt.dateTo;
        this.lng = newEvt.lng;
        this.lat = newEvt.lat;
        
    }


    makeMarker(map) {
        let color = '#2ac644';

        const dateNow = Date.now();

        const delta = this.dateTo - dateNow;
        
        
        if( delta < 0 ) {
            color = '#F80400';
            
        }else if(delta < (3 * 24 * 3600 * 1000)){ 
            color = '#F1A517';
            
        }
        

        const marker = new mapbox.Marker({ color }); 
        
        marker.getElement().title = this.title;
        marker.getElement().description = this.description;
        marker.getElement().dateFrom = this.dateFrom;
        marker.getElement().dateTo = this.dateTo;


        const popup = new mapbox.Popup();
        
        popup.setHTML( 
            `<div class="content-wrapper">
                    
                    <div class="title"></div>
                    <div class="content"></div>
                    <h3 class="h3-border">Date de début :</h3>
                    <div class="timeFrom"></div>
                    <h3>Date de Fin :</h3>
                    <div class="timeTo"></div>
            </div>
                                        `);

            this.domContainer = popup._content;
            
            
            this.domTitle = this.domContainer.querySelector( '.title' );
            this.domTitle.textContent = this.title;
                                
            this.domContent = this.domContainer.querySelector( '.content' );
            this.domContent.textContent = this.description;
    
            this.domTime = this.domContainer.querySelector('.timeFrom');
            

            this.domTime2 = this.domContainer.querySelector('.timeTo');
            
            
            this.domTime.textContent =  new Date(this.dateFrom);
            this.domTime2.textContent = new Date(this.dateTo);
        
        
        // "du " + this.dateFrom + "au " + this.dateTo

        marker.setPopup( popup );
        
        marker.setLngLat([ this.lng, this.lat ]);
        marker.addTo(map);

        
    }

    toJSON() {
        return {
            title: this.title,
            description: this.description,
            dateFrom: this.dateFrom,
            dateTo: this.dateTo,
            lng: this.lng,
            lat: this.lat
        }
    }

    

    
    
};
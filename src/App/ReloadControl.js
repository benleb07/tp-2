import app from './App';

export class ReloadControl {
    onAdd(map) {
    this._map = map;
    this._container = document.createElement('div');
    this._container.className = 'mapboxgl-ctrl';
    
    const btnRefresh = document.createElement( 'button' );
    btnRefresh.classList.add('refresh-btn');
    btnRefresh.innerHTML = 'Rafraîchir';
    
    function refreshBtn(){
        location.reload();
    }

    btnRefresh.addEventListener( 'click', refreshBtn );
    this._container.appendChild( btnRefresh );

    return this._container;
    }
    
    onRemove() {
    this._container.parentNode.removeChild(this._container);
    this._map = undefined;
    }
}
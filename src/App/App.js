import '../index.html';
import 'mapbox-gl/dist/mapbox-gl.css';
import '../style.css';


import appConfig from '../../app.config';

import { LocalEvt } from '../App/LocalEvt';

import { ReloadControl } from './ReloadControl';


const mapbox = require('mapbox-gl/dist/mapbox-gl.js');


class App {
    
    map;
    domEvtTitle;
    domEvtDescription;
    domBtnNewAdd;
    domTime;
    domTime2;
    domList;
    domEvtLng;
    domEvtLat;

    markersList = [];
    
    

    start() {
        // Initialisation de la map        
        console.log('app démarée');
        this.linkHtmlCss();
        this.pingMarker();
        this.loadEvtList();
        this.loadMarkers();
    };

    linkHtmlCss() {
        mapbox.accessToken = "pk.eyJ1IjoiYmVubGViMDciLCJhIjoiY2tuZm93aWM5MTl2ajJwbzdnZG1xM2FmdSJ9.vA8s6CyCu1YYZDszcFalow";

        

        this.map = new mapbox.Map({
            container: 'map',
            center: { lng: 8.021157, lat: 48.919946 },
            zoom: 5,
            minZoom: 5,
            style: 'mapbox://styles/mapbox/streets-v11',
            maxBounds: [[-10.357555,41.586370],[14.056663, 51.516677]],
            
        });


        this.domEvtTitle = document.querySelector('#evt-title');
        this.domEvtTitle.addEventListener( 'focus', this.onErrorRemove.bind(this));

        this.domEvtDescription = document.querySelector('#evt-description');
        this.domEvtDescription.addEventListener( 'focus', this.onErrorRemove.bind(this));

        this.domTime = document.querySelector('#date-time');

        this.domTime2 = document.querySelector('#date-time2');
        
        this.domBtnNewAdd = document.querySelector('#new-add');
        this.domBtnNewAdd.addEventListener( 'click', this.onBtnAddClick.bind(this));


        this.domEvtLng = document.querySelector('#evt-lng');
        this.domEvtLat = document.querySelector('#evt-lat');

        // Bouton de zoom
        const zoomCtrl = new mapbox.NavigationControl();
        this.map.addControl( zoomCtrl );

        const iControl = new ReloadControl();
        this.map.addControl( iControl, 'top-left' );

        
    };
    

    pingMarker() {
        // Ecouteur d'évènement 'click'
        this.map.on( 'click', this.onMapClick.bind(this));
        
        
    };

    onMapClick(evt){
        
        console.log(evt);
        
        const lngCoor = evt.lngLat.lng;
        const latCoor = evt.lngLat.lat;
        
        this.domEvtLng.value = lngCoor;

        this.domEvtLat.value = latCoor;

        
    };

    loadEvtList(){
        const storageContent = localStorage.getItem( appConfig.localStorageName );
        if( storageContent === null ) {
            return;
        }

        let savedMarkers;

        try {
            savedMarkers = JSON.parse(storageContent);
        }
        catch(error){
            localStorage.removeItem ( appConfig.localStorageName );
            return;
        }

        for(let newEvt of savedMarkers) {
            const newMarkers = new LocalEvt( newEvt );
            this.markersList.push( newMarkers );
        }

        
    };

    onBtnAddClick(){
        let onError = false;
        const evtTitle = this.domEvtTitle.value.trim();
        if( evtTitle === '' ) {
            this.domEvtTitle.classList.add( 'error' );
            this.domEvtTitle.value = '';
            onError = true;
        };


        const evtDescription = this.domEvtDescription.value.trim();
        if( evtDescription === '' ) {
            this.domEvtDescription.classList.add( 'error' );
            this.domEvtDescription.value = '';
            onError = true;
        };

        if( onError ){
            return;
        };

        let startDate = new Date(this.domTime.value);
        
        let endDate = new Date(this.domTime2.value);


        const newMarker = {
            title: this.domEvtTitle.value,
            description: this.domEvtDescription.value,
            dateFrom: startDate.getTime(),
            dateTo: endDate.getTime(),
            lng: this.domEvtLng.value,
            lat: this.domEvtLat.value,
        };

        

        this.domEvtTitle.value = this.domEvtDescription.value = '';

        const newPing = new LocalEvt( newMarker );
        newPing.makeMarker(this.map);


        this.markersList.push ( newPing );

        localStorage.setItem( appConfig.localStorageName, JSON.stringify( this.markersList ) );
        

        
    };

    onErrorRemove( evt ) {
        evt.target.classList.remove( 'error' );
    };

    

    loadMarkers() {
        for(const localEvt of this.markersList){
            localEvt.makeMarker(this.map);
            
        }
    }

    
    
}

const instance = new App();

export default instance;